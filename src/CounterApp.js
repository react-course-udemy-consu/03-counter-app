import React, { useState } from 'react';
import { PropTypes } from "prop-types";

const CounterApp = ({value}) => {
  //Un hook es una función
  const [counter, setCounter] = useState(value);
  //La función useState retorna un arreglo []

  //Función handleAdd para el boton que le va a entregar un 
  //evento (e) como argumento
  const handleAdd = () => {
    setCounter(counter + 1);
    //otra forma de utilizar el setCounter sería así:
    /* setCounter((c) => c + 1 ); */
  }

  //Tarea, agregar 2 botones para resetear el valor y restar 1
  const handleSubtract = () => {
    setCounter(counter - 1);
  }

  const handleRest = () => {
    setCounter(value);
  }

  return(
    <>
      <h1>CounterApp</h1>
      <h2>{ counter }</h2>
      <button onClick={ handleAdd }>+1</button> 
      <button onClick={ handleRest }>Reset</button> 
      <button onClick={ handleSubtract }>-1</button>
    </>
  );
}

CounterApp.propTypes = {
  value: PropTypes.number
}

export default CounterApp;


